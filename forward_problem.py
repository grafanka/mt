import numpy as np
import files_read_write as frw


global mu
mu = 4*np.pi*1e-07


def arcoth(z):
    return 0.5*np.log((z+1)/(z-1))


def coth(z):
    return 1/(np.tanh(z))


def arcoth_dz(z):
    """Returns derivate of arcoth"""

    return 1/(1-(z*z))


def coth_dz(z):
    """Returns derivate of coth"""

    return -pow(2/(np.exp(z)-np.exp(-z)), 2)


def k(ro, f):
    return np.sqrt((1j * 2 * np.pi * f * mu) / ro)


def c(ki, hi, ci):

    if ki * ci == 1:

        return (1/ki)

    else:
        return (1/ki)*coth(ki*hi+arcoth(ki*ci))


def c_for_all_layers(ro, h, f):
    """Returns C on surface at frequency 'f' for model [h], [ro] """

    m = len(ro)-1
    cn = 1/k(ro[m], f)                        # C at the last border
    ci = cn
    m -= 1
    res = [ci]

    while m >= 0:

        ci = c(k(ro[m], f), h[m], ci)
        res.append(ci)
        m -= 1

    res.reverse()
    return res


def dcj_dkj(c_for_ro, ro, h, f, j):
    """Returns derivate dcj/dkj. Parametrs: <c_for_ro> list of all C for freqoency <f>;
     lists of model's <ro> and <h>; <f> one frequency; <j> number of derivate."""

    kj = k(ro[j], f)

    return (-1/(kj*kj))*coth(kj*h[j] + arcoth(kj*c_for_ro[j+1]))\
        + (1/kj)*coth_dz(kj*h[j] + arcoth(kj*c_for_ro[j+1]))\
        * (h[j] + (arcoth_dz(kj*c_for_ro[j+1])) * c_for_ro[j+1])


def dc0_droj(c_for_ro, ro, h, f, j):
    """Returns derivate dc0/dkj (c0 - c on surface)."""

    return -0.5*np.sqrt((1j*2*np.pi*f*mu)/pow(ro[j], 3))*dcj_dkj(c_for_ro, ro, h, f, 0)


def dro_droj(c_for_ro, ro, h, f):
    """Returns derivate dro_droj for all <ro> at one <f>"""

    # dc_dr = list(range(len(ro)))
    dr_droj = list(range(len(ro)))
    c0 = c_for_all_layers(ro, h, f)[0]

    for j in range(len(ro)-1):

        dc0_dr = dc0_droj(c_for_ro, ro, h, f, j)
        dr_droj[j] = 2*np.pi*f*mu*(((c0.real) * (dc0_dr.real)) + ((c0.imag) * (dc0_dr.imag)))


    return dr_droj


def jacobian(ro, h, f):

    jacob = [dro_droj(c_for_all_layers(ro, h, x), ro, h, x) for x in f]

    return np.array(jacob)


def cn_for_all_freq(ro, h, f, n):

    return [c_for_all_layers(ro, h, x)[n] for x in f]


def ro_list(c_list, f):
    """Calculate Ro from C at all frequency from list [f] """

    ro = []

    for i in range(len(c_list)):
        ro.append(2*np.pi*f[i]*mu*pow(np.abs(c_list[i]), 2))

    return ro


def solve_forward(ro, h, f):

    return ro_list(cn_for_all_freq(ro, h, f, 0), f)


def main():

    ro = frw.read_data('model_start_1.csv')
    h = [3.75 for x in range(len(ro)-1)]
    f = frw.read_data('freq.csv')

    #
    # ro = [1000, 10, 1000]
    # h = [50, 100]




if __name__ == '__main__':
        main()
import numpy as np

import forward_problem as fp
import files_read_write as frw
import plot as plt


def w(n):
    """Returns matrix n*n with 1 on diagonals"""

    e = np.array([[0 for x in range(n)] for x in range(n)])
    m = 0
    for x in e:
        x[m] = 1
        m += 1
    return e


def d(n):

    res = np.array([[0 for x in range(n)] for x in range(n)])
    n = -1
    for x in res:
        if n != -1:
            x[n] = -1
            x[n+1] = 1
        n += 1
    return res


def mu():

    x = 1
    res = [x]
    while x < 10000:
        x *= pow(10, 0.1)
        res.append(x)
    return res


def mod_k(ro, h, f, data, mu):

    n = len(ro)
    m = len(data)

    wj = np.dot(w(m), fp.jacobian(ro, h, f))

    data_new = data - fp.solve_forward(ro, h, f) + np.dot(fp.jacobian(ro, h, f), ro)

    inv = np.linalg.inv(mu * np.dot(d(n).T, d(n)) + np.dot(wj.T, wj))

    res = np.dot(inv, wj.T)

    wd = np.dot(w(m), data_new)

    res = np.dot(res, wd)

    return res


def new_model(ro, h, f, data, mu):

    all = np.array([fp.solve_forward(mod_k(ro, h, f, data, x), h, f) for x in mu])

    misfit = []

    for x in all:
        misf = np.linalg.norm(np.dot(w(len(data)), data) - np.dot(w(len(data)), x))
        misfit.append(misf)

    i = misfit.index(min(misfit))

    return mod_k(ro, h, f, data, mu[i])


def inversion(ro, h, f, data, mu, iter):

    res = []
    while iter > 0:
        mod = new_model(ro, h, f, data, mu)
        ro = mod
        res.append(mod)
        iter -= 1

    return np.array(res)


def main():

    ro = frw.read_data('model_start.csv')
    data = frw.read_data('data.csv')
    h = [3.75 for x in range(len(ro) - 1)]
    f = frw.read_data('freq.csv')

    inv_models = inversion(ro, h, f, data, mu(), 1)

    frw.write_results('results.csv', data, fp.solve_forward(ro, h, f), [fp.solve_forward(x, h, f) for x in inv_models])


if __name__ == '__main__':
        main()

import forward_problem as fp
import unittest


class TestsFP(unittest.TestCase):

    def test_abs(self):

        self.assertEqual(fp.abs(5+0.007j),  5.000004899997599)

    def test_cth(self):

        self.assertEqual(fp.cth(1+1j), 0.8680-0.2176j)

    def test_arcth(self):

        self.assertEqual(fp.arcth(1j), -0.785398163367869j)


if __name__ == '__main__':
    unittest.main()
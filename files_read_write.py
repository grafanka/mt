import numpy as np
import csv


def read_data(filename):

    array = np.genfromtxt(filename, delimiter=',')

    return array


def write_data(filename, lists):

    with open(filename, 'w', newline='') as csv_file:
        csv_writer = csv.writer(csv_file)

        for x in lists:
            csv_writer.writerow(x)


def write_results(filename, data, start_model, inversions):

    result = [list(data)] + [list(start_model)]

    for x in inversions:

        result.append(x)

    write_data(filename, result)

